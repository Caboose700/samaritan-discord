package net.gamechaser.samaritanDiscord.apiTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.gamechaser.samaritanDiscord.Credentials;
import net.gamechaser.samaritanDiscord.helpers.DiscordHelper;
import net.gamechaser.samaritanDiscord.helpers.HTTPHelper;

public class Twitch 
{
	
	public static ArrayList<Integer> check(String channelID, Map<Integer, String> names, Map<Integer, String> urls, ArrayList<Integer> knownData) throws ParseException, IOException, InterruptedException {
		
		// Init Result Array
		// For each person streaming, add them to the Result Array
		// For each person on the Result Array, if they are not on the knownData array, print message
		// Return Result Array
		
		// Build Result
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		String twitchJSON = HTTPHelper.download(Credentials.getTwitchURL());
		if (twitchJSON.equals("Connection Failed")) 
		{
			return null; // Check for this after each run
		}
		
		// Debug Message
		//System.out.println("Downloaded JSON");
		
		// Get Root Objects
		JSONParser twitchParser = new JSONParser();
		JSONObject rootObject = (JSONObject) twitchParser.parse(twitchJSON);
		
		// Get Total Number of Streaming Clients
		int total = Integer.parseInt(rootObject.get("_total").toString());
		
		// Debug Message
		//System.out.println("Got Total Number of Streams");
		
		if (total > 0) 
		{
			JSONArray streams = (JSONArray) rootObject.get("streams");
			for (int i = 0; i < streams.size(); i++) 
			{
				// Get ID of Streaming Party
				JSONObject tempStream = (JSONObject) streams.get(i);
				JSONObject tempChannel = (JSONObject) tempStream.get("channel");
				int streamerID = Integer.parseInt(tempChannel.get("_id").toString());
				
				// Check to see if streamerID is located in knownData
				// If yes, we have already reported this person to the chat
				// If no, we haven't and we should add this to our result
				
				result.add(streamerID);	
			}
			
			// Debug Message
			//System.out.println("Checking if we reported this id already");
			
			// If streamerID exists on knownData and on result, remove them from result.
			for (int i = 0; i < result.size(); i++) 
			{
				if(!knownData.contains(result.get(i))) 
				{
					DiscordHelper.write(channelID, "``[Twitch]`` " + names.get(result.get(i)) + " is currently streaming! " + urls.get(result.get(i)));
				}
			}
		}
		
		// Debug Message
		//System.out.println("Reporting Result.");
		
		// If there are people still streaming, return the ID's of those who are. If not, we will now return an Empty ArrayList
		return result;
	}

	// Overloader for Above Function
	public static ArrayList<Integer> check(String channelID, Map<Integer, String> names, Map<Integer, String> urls) throws ParseException, IOException, InterruptedException 
	{
		ArrayList<Integer> knownData = new ArrayList<Integer>();
		return check(channelID, names, urls, knownData);
	}
}

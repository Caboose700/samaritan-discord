package net.gamechaser.samaritanDiscord.apiTools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.unbescape.html.HtmlEscape;
import org.unbescape.uri.UriEscape;

import net.gamechaser.samaritanDiscord.helpers.HTTPHelper;

public class GoogleImageSearch 
{
	public static String[] search(String searchQuery) throws UnsupportedEncodingException, ParseException 
	{
		String urlQuery = URLEncoder.encode(searchQuery, "UTF-8");
		String[] ConnectionFailed = {"``[Google Image]`` Connection Error. Please try again in a few moments."};
		String[] ImageFailed = {"``[Google Image]`` Image Not Found."};
	
		String googleJSON = HTTPHelper.download("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + urlQuery);
		if (googleJSON.equals("Connection Failed")) 
		{
			return ConnectionFailed;
		}
		
		ArrayList <String> googleSearchMessagesList = new ArrayList<String>();
		
		JSONParser googleSearchParser = new JSONParser();
		JSONObject resultObject = (JSONObject) googleSearchParser.parse(googleJSON);
		
		JSONObject googleSearchResponseData = (JSONObject) resultObject.get("responseData");
		if (googleSearchResponseData == null) 
		{
			return ImageFailed;
		}
		
		JSONArray googleSearchResults = (JSONArray) googleSearchResponseData.get("results");
		if (googleSearchResults.size() == 0) 
		{
			return ImageFailed;
		}
		
		JSONObject googleSearchResult = (JSONObject) googleSearchResults.get(0);
		
		String googleUrl = googleSearchResult.get("url").toString();
		String googleTitle = googleSearchResult.get("titleNoFormatting").toString();
		String googleDescription = googleSearchResult.get("content").toString();
		String imageWidth = googleSearchResult.get("width").toString();
		String imageHeight = googleSearchResult.get("height").toString();
		
		String urlClean = UriEscape.unescapeUriPath(googleUrl);
		String titleClean = HtmlEscape.unescapeHtml(googleTitle);
		String descriptionClean = HtmlEscape.unescapeHtml(googleDescription.replaceAll("\\r\\n|\\r|\\n", "").replaceAll("[^\\x00-\\x7F]","").replaceAll("\\<.*?>",""));
		
		googleSearchMessagesList.add("``[Google Image]`` " + titleClean + " <" + urlClean + ">");
		googleSearchMessagesList.add("``[Google Image]`` Size: " + imageWidth + "x" + imageHeight + "px - Description: " + descriptionClean);
		
		String[] googleSearchResponse = new String[googleSearchMessagesList.size()];
		googleSearchResponse = googleSearchMessagesList.toArray(googleSearchResponse);
		return googleSearchResponse;
	}
}

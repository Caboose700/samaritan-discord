package net.gamechaser.samaritanDiscord.apiTools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.gamechaser.samaritanDiscord.Credentials;
import net.gamechaser.samaritanDiscord.helpers.HTTPHelper;

public class Weather 
{
	public static String[] get(String searchQuery) throws UnsupportedEncodingException, ParseException 
	{
		String urlQuery = URLEncoder.encode(searchQuery, "UTF-8");
		String[] CityFailed = {"``[Weather]`` City Not Found."};
		String[] ConnectionFailed = {"``[Weather]`` Connection Error. Please try again in a few moments."};
		
		String weatherJSON = HTTPHelper.download("http://api.openweathermap.org/data/2.5/weather?q=" + urlQuery + "&APPID=" + Credentials.getOpenWeatherMapAPI());
		if (weatherJSON.equals("Connection Failed")) 
		{
			return ConnectionFailed;
		}
		
		ArrayList<String> weatherMessageList = new ArrayList<String>();
		
		JSONParser weatherQueryParser = new JSONParser();
		JSONObject resultObject = (JSONObject) weatherQueryParser.parse(weatherJSON);
		if (resultObject == null) 
		{
			return CityFailed;
		}
		String code = resultObject.get("cod").toString();
		if (!code.equals("200")) 
		{
			return CityFailed;
		}
		
		JSONArray weatherInfo = (JSONArray) resultObject.get("weather");
		JSONObject mainWeather = (JSONObject) weatherInfo.get(0);
		JSONObject weatherTemp = (JSONObject) resultObject.get("main");
		JSONObject weatherSys = (JSONObject) resultObject.get("sys");
		
		String cityID = resultObject.get("id").toString();
		String cityName = resultObject.get("name").toString();
		String countryCode = weatherSys.get("country").toString();
		String weatherConditions = mainWeather.get("main").toString();
		
		float weatherTempK = Float.parseFloat(weatherTemp.get("temp").toString());
		
		float weatherPressure = 0f;
		if (weatherTemp.get("pressure") != null) 
		{
			weatherPressure = Float.parseFloat(weatherTemp.get("pressure").toString());
		}

		int weatherHumidity = 0;
		if (weatherTemp.get("humidity") != null) 
		{
			weatherHumidity = Integer.parseInt(weatherTemp.get("humidity").toString());
		}
		
		float weatherTempF = (float) ((weatherTempK * 1.8) - 459.67);
		float weatherTempC = (float) (weatherTempK - 273.15);
		
		String weatherTempFString = String.format("%.1f", weatherTempF);
		String weatherTempCString = String.format("%.1f", weatherTempC);
		
		weatherMessageList.add("``[Weather]`` " + cityName + ", " + countryCode + " | Temp: " + weatherTempCString + " C  " + weatherTempFString + " F");
		weatherMessageList.add("``[Weather]`` Conditions: " + weatherConditions + " | Humidity: " + weatherHumidity + "% | Pressure: " + weatherPressure + " hPa");
		weatherMessageList.add("``[Weather]`` More Information: http://openweathermap.com/city/" + cityID);
		
		String[] weatherMessageResponse = new String[weatherMessageList.size()];
		weatherMessageResponse = weatherMessageList.toArray(weatherMessageResponse);
		return weatherMessageResponse;
	}
}
package net.gamechaser.samaritanDiscord.apiTools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Trello 
{
	public static String generateTime() 
	{
	    TimeZone tz = TimeZone.getTimeZone("UTC");
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    df.setTimeZone(tz);
	    return df.format(new Date());
	}
	
	public static void recordEndTime() throws IOException 
	{
		Writer output;
		output = new BufferedWriter(new FileWriter("date.txt", false));
		output.append(generateTime());
		output.close();
	}
	
	public static String getEndTime() throws IOException 
	{
		BufferedReader input = new BufferedReader(new FileReader("date.txt"));
		String Result = input.readLine().toString();
		input.close();
		return Result;
	}
}

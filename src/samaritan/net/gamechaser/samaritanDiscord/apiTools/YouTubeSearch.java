package net.gamechaser.samaritanDiscord.apiTools;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.gamechaser.samaritanDiscord.Credentials;
import net.gamechaser.samaritanDiscord.helpers.HTTPHelper;

public class YouTubeSearch 
{
	public static String[] search(String searchQuery) throws ParseException, UnsupportedEncodingException 
	{
		String urlQuery = URLEncoder.encode(searchQuery, "UTF-8");
		String[] VideoFailed = {"``[YouTube]`` Video Not Found."};
		String[] ConnectionFailed = {"``[YouTube]`` Connection Error. Please try again in a few moments."};
		String[] InvalidQuery = {"``[YouTube]`` Your search term did not return any results. Please try a different one."};
		
		String youtubeJSON = HTTPHelper.download("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=2&q=" + urlQuery + "&key=" + Credentials.getYouTubeAPI());
		if (youtubeJSON.equals("Connection Failed")) 
		{
			return ConnectionFailed;
		}
		
		ArrayList<String> youtubeMessageList = new ArrayList<String>();
		
		JSONParser youtubeSearchParser = new JSONParser();
		JSONObject youtubeSearchResult = (JSONObject) youtubeSearchParser.parse(youtubeJSON);
		
		JSONArray youtubeSearchItems = (JSONArray) youtubeSearchResult.get("items");
		if (youtubeSearchItems.size() == 0) 
		{
			return VideoFailed;
		}
		
		JSONObject youtubeItem = (JSONObject) youtubeSearchItems.get(0);
		JSONObject youtubeItemID = (JSONObject) youtubeItem.get("id");
		
		String youtubeResultKind = youtubeItemID.get("kind").toString();
		if (youtubeResultKind.equals("youtube#channel") || youtubeResultKind.equals("youtube#playlist")) 
		{
			youtubeItem = (JSONObject) youtubeSearchItems.get(1);
			youtubeItemID = (JSONObject) youtubeItem.get("id");
			youtubeResultKind = youtubeItemID.get("kind").toString();
			if (youtubeResultKind.equals("youtube#channel") || youtubeResultKind.equals("youtube#playlist")) 
			{
				return InvalidQuery;
			}
		}
		
		JSONObject youtubeItemInfo = (JSONObject) youtubeItem.get("snippet");
		
		String youtubeVideoID = youtubeItemID.get("videoId").toString();
		String youtubeVideoURL = "https://www.youtube.com/watch?v=" + youtubeVideoID;
		String youtubeVideoTitle = youtubeItemInfo.get("title").toString();
		
		// Get Video Statistics
		String youtubeStatJSON = HTTPHelper.download("https://www.googleapis.com/youtube/v3/videos?part=statistics,contentDetails&id=" + youtubeVideoID + "&key=" + Credentials.getYouTubeAPI());
		if (youtubeStatJSON.equals("Connection Failed")) 
		{
			return ConnectionFailed;
		}
		
		JSONParser youtubeStatParser = new JSONParser();
		JSONObject youtubeStatResult = (JSONObject) youtubeStatParser.parse(youtubeStatJSON);
		
		JSONArray youtubeStatItems = (JSONArray) youtubeStatResult.get("items");
		if (youtubeStatItems.size() == 0) 
		{
			return VideoFailed;
		}
		
		JSONObject youtubeStat = (JSONObject) youtubeStatItems.get(0);
		JSONObject youtubeDetails = (JSONObject) youtubeStat.get("contentDetails");
		JSONObject youtubeStats = (JSONObject) youtubeStat.get("statistics");
		
		String youtubeVideoDuration = youtubeDetails.get("duration").toString();
		
		long youtubeVideoViewsNum = Long.parseLong(youtubeStats.get("viewCount").toString());
		int youtubeVideoLikesNum = Integer.parseInt(youtubeStats.get("likeCount").toString());
		int youtubeVideoDislikesNum = Integer.parseInt(youtubeStats.get("dislikeCount").toString());
		
		String youtubeVideoViews = NumberFormat.getNumberInstance(Locale.US).format(youtubeVideoViewsNum);
		String youtubeVideoLikes = NumberFormat.getNumberInstance(Locale.US).format(youtubeVideoLikesNum);
		String youtubeVideoDislikes = NumberFormat.getNumberInstance(Locale.US).format(youtubeVideoDislikesNum);
		
		youtubeMessageList.add("``[YouTube]`` " + youtubeVideoTitle + " (" + getTimeFromString(youtubeVideoDuration) + ")");
		youtubeMessageList.add("``[YouTube]`` " + youtubeVideoURL);
		youtubeMessageList.add("``[YouTube]`` Views: " + youtubeVideoViews + " | Likes: " + youtubeVideoLikes + " | Dislikes: " + youtubeVideoDislikes);
		
		String[] youtubeMessageResponse = new String[youtubeMessageList.size()];
		youtubeMessageResponse = youtubeMessageList.toArray(youtubeMessageResponse);
		return youtubeMessageResponse;
	}

	public static String[] retrieve(String videoID) throws ParseException 
	{
		ArrayList<String> youtubeResponse = new ArrayList<String>();
		String[] VideoFailed = {"``[YouTube]`` Video Not Found."};
		String[] ConnectionFailed = {"``[YouTube]`` Connection Error. Please try again in a few moments."};
		
		String youtubeRetrieveJSON = HTTPHelper.download("https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics,contentDetails&id=" + videoID + "&key=" + Credentials.getYouTubeAPI());
		if (youtubeRetrieveJSON.equals("Connection Failed")) 
		{
			return ConnectionFailed;
		}
		
		JSONParser youtubeRetrieveParser = new JSONParser();
		JSONObject youtubeRetrieveResult = (JSONObject) youtubeRetrieveParser.parse(youtubeRetrieveJSON);
		
		JSONArray youtubeItems = (JSONArray) youtubeRetrieveResult.get("items");
		if (youtubeItems.size() == 0) 
		{
			return VideoFailed;
		}
		
		JSONObject youtubeStats = (JSONObject) youtubeItems.get(0);
		JSONObject youtubeInfo = (JSONObject) youtubeStats.get("snippet");
		JSONObject youtubeDetails = (JSONObject) youtubeStats.get("contentDetails");
		JSONObject youtubeStatistics = (JSONObject) youtubeStats.get("statistics");
		
		String videoTitle = youtubeInfo.get("title").toString();
		String channelTitle = youtubeInfo.get("channelTitle").toString();
		String videoDuration = youtubeDetails.get("duration").toString();
		
		long videoViewCount = Long.parseLong(youtubeStatistics.get("viewCount").toString());
		int videoLikeCount = Integer.parseInt(youtubeStatistics.get("likeCount").toString());
		int videoDislikeCount = Integer.parseInt(youtubeStatistics.get("dislikeCount").toString());
		
		String youtubeVideoViews = NumberFormat.getNumberInstance(Locale.US).format(videoViewCount);
		String youtubeVideoLikes = NumberFormat.getNumberInstance(Locale.US).format(videoLikeCount);
		String youtubeVideoDislikes = NumberFormat.getNumberInstance(Locale.US).format(videoDislikeCount);
		
		youtubeResponse.add("``[YouTube]`` " + videoTitle + " (" + getTimeFromString(videoDuration) + ")");
		youtubeResponse.add("``[YouTube]`` Channel Name: " + channelTitle);
		youtubeResponse.add("``[YouTube]`` Views: " + youtubeVideoViews + " | Likes: " + youtubeVideoLikes + " | Dislikes: " + youtubeVideoDislikes);
		
		String[] youtubeReturn = new String[youtubeResponse.size()];
		youtubeReturn = youtubeResponse.toArray(youtubeReturn);
		return youtubeReturn;
	}
	
	private static String getTimeFromString(String duration) {
		String time = "";
	    boolean hourexists = false, minutesexists = false, secondsexists = false;
	    if (duration.contains("H"))
	        hourexists = true;
	    if (duration.contains("M"))
	        minutesexists = true;
	    if (duration.contains("S"))
	        secondsexists = true;
	    if (hourexists) {
	        String hour = "";
	        hour = duration.substring(duration.indexOf("T") + 1,
	                duration.indexOf("H"));
	        if (hour.length() == 1)
	            hour = "0" + hour;
	        time += hour + ":";
	    }
	    if (minutesexists) {
	        String minutes = "";
	        if (hourexists)
	            minutes = duration.substring(duration.indexOf("H") + 1,
	                    duration.indexOf("M"));
	        else
	            minutes = duration.substring(duration.indexOf("T") + 1,
	                    duration.indexOf("M"));
	        if (minutes.length() == 1)
	            minutes = "0" + minutes;
	        time += minutes + ":";
	    } else {
	        time += "00:";
	    }
	    if (secondsexists) {
	        String seconds = "";
	        if (hourexists) {
	            if (minutesexists)
	                seconds = duration.substring(duration.indexOf("M") + 1,
	                        duration.indexOf("S"));
	            else
	                seconds = duration.substring(duration.indexOf("H") + 1,
	                        duration.indexOf("S"));
	        } else if (minutesexists)
	            seconds = duration.substring(duration.indexOf("M") + 1,
	                    duration.indexOf("S"));
	        else
	            seconds = duration.substring(duration.indexOf("T") + 1,
	                    duration.indexOf("S"));
	        if (seconds.length() == 1)
	            seconds = "0" + seconds;
	        time += seconds;
	    }
	    return time;
	}
}

package net.gamechaser.samaritanDiscord.helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPHelper 
{
	public static String download (String sUrl) 
	{
		try 
		{
		    URL url = new URL(sUrl);
		    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		    connection.setRequestMethod("GET");
		    connection.setDoOutput(true);
		    connection.setConnectTimeout(5000);
		    connection.setReadTimeout(5000);
		    connection.connect();
		    BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		    String content = "", line;
		    while ((line = rd.readLine()) != null) 
		    {
		        content += line + "\n";
		    }
		    return content;
		} 
		catch (Exception e) 
		{
			return "Connection Failed";
		}
	}
}

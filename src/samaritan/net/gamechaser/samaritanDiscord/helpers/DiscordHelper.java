package net.gamechaser.samaritanDiscord.helpers;

import java.io.IOException;

import sx.blah.discord.util.MessageBuilder;

public class DiscordHelper 
{
	public static void write(String channelID, String message) throws IOException 
	{
		new MessageBuilder().appendContent(message).withChannel(channelID).build();
	}
	
	public static void write(String channelID, String[] message) throws IOException, InterruptedException 
	{
		MessageBuilder newMessage = new MessageBuilder();
		
		for (int i = 0; i<message.length; i++) 
		{
			newMessage.appendContent(message[i] + "\\n");
		}
		
		newMessage.withChannel(channelID).build();
	}
	
	public static boolean commandCheck(String message)
	{
		String[] commands = {"!define ", "!w ", "!g ", "!gi ", "!yt ", "!help", "!igg", "!info-samaritan", "!twitch"};
		for (int i = 0; i < commands.length; i++)
		{
			if (message.startsWith(commands[i])) {
				return true;
			}
		}
		return false;
	}
}
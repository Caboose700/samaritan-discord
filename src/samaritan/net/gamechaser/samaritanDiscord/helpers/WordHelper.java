package net.gamechaser.samaritanDiscord.helpers;

public class WordHelper 
{
	public static String capitalizeFirst(String string) 
	{
		if (string.length() > 0) 
		{
			return string.substring(0,1).toUpperCase() + string.substring(1);
		}
		return string;
	}
	
	public static String capitalizeEvery(String string) 
	{
		String[] words = string.split("\\s");
		StringBuilder builder = new StringBuilder();
		for (int i=0; i<words.length; i++) 
		{
			builder.append(Character.toUpperCase(words[i].charAt(0)));
			builder.append(words[i].substring(1));
			if (i < words.length - 1) 
			{
				builder.append(" ");
			}
		}
		return builder.toString();
	}
	
	public static String makeSentence(String string) 
	{
		return capitalizeFirst(string) + ".";
	}
	
	public static String endList(String string, boolean sentenceEnd) 
	{
		if (string.length() > 2) 
		{
			if (sentenceEnd) 
			{
				return string.substring(0, string.length() - 2) + ".";
			}
			return string.substring(0, string.length() - 2);
		}
		return string;
	}
}
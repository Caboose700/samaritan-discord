package net.gamechaser.samaritanDiscord.helpers;

import java.io.FileInputStream;

import org.apache.commons.net.ftp.FTPClient;

public class FTPHelper 
{
	public static void upload(String filename, String FTPServer, String FTPUsername, String FTPPassword, String destination, int TransferType) 
	{
		try 
		{
			FTPClient client = new FTPClient();
			FileInputStream fis = null;
			client.connect(FTPServer);
			client.login(FTPUsername, FTPPassword);
			fis = new FileInputStream(filename);
			client.setFileType(TransferType, TransferType);
			client.storeFile(destination, fis);
			fis.close();
			client.logout();
			return;
		} 
		catch (Exception e) 
		{
			System.out.println("Failed to upload to FTP Server");
			return;
		}
	}
}

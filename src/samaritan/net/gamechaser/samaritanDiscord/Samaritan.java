package net.gamechaser.samaritanDiscord;

import net.gamechaser.samaritanDiscord.apiTools.Dictionary;
import net.gamechaser.samaritanDiscord.apiTools.GoogleImageSearch;
import net.gamechaser.samaritanDiscord.apiTools.GoogleSearch;
import net.gamechaser.samaritanDiscord.apiTools.Trello;
import net.gamechaser.samaritanDiscord.apiTools.Twitch;
import net.gamechaser.samaritanDiscord.apiTools.Weather;
import net.gamechaser.samaritanDiscord.apiTools.YouTubeSearch;
import net.gamechaser.samaritanDiscord.helpers.DiscordHelper;
import sx.blah.discord.DiscordClient;
import sx.blah.discord.handle.*;
import sx.blah.discord.handle.impl.events.*;
import sx.blah.discord.handle.obj.Message;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

public class Samaritan 
{
	public static void main(String[] args) 
	{
		String[] indiegogo = {
		"Tower Unite Indiegogo Final Total:", 
		"$73,633 USD raised by 2,051 people.", 
		"https://www.indiegogo.com/projects/tower-unite"
		};
		
		String[] info = {
		"``Samaritan Version:`` 0.0.2",
		"``System Administrator:`` Caboose700",
		"``Primary    Mandate:`` Detect Trello Updates", 
		"``Secondary  Mandate:`` Detect Pixeltail Developer Twitch Streams",
		"``Tertiary   Mandate:`` Provide Tools for Discord Users"
		};
		
		String[] helpMessage = {
		"Operational Directives:",
		"-----------------------",
		"``!igg``    (Provides Indiegogo Information for Tower Unite)",
		"``!twitch`` (Checks to see if any Pixeltail Developers are Streaming)",
		"``!info-samaritan`` (Displays Information about Samaritan)",
		"``!g`` (Google Search)        | Syntax: !g  *SEARCH TERM HERE*",
		"``!gi`` (Google Image Search) | Syntax: !gi *SEARCH TERM HERE*",
		"``!yt`` (Youtube Search)      | Syntax: !yt *SEARCH TERM HERE*",
		"``!w`` (Weather Search)       | Syntax: !w  *CITY,COUNTRY*",
		"``!define`` (Dictionary)      | Syntax: !define *WORD HERE*"
		};
		
		ArrayList<String> LoginInfo = Credentials.DiscordCredentials();
		String Username = LoginInfo.get(0);
		String Password = LoginInfo.get(1);
		String channelID = LoginInfo.get(2);
		
		// Twitch Names
		Map <Integer, String> TwitchNames = new HashMap<Integer, String>();
		TwitchNames.put(88752545, "Pixeltail Games");
		TwitchNames.put(2185137,  "MacDGuy");
		TwitchNames.put(6854939,  "Foohy");
		TwitchNames.put(32039860, "Matt");
		TwitchNames.put(22899421, "Lifeless");
		TwitchNames.put(46510960, "Zak");
		
		// Twitch URLs
		Map <Integer, String> TwitchURLs = new HashMap<Integer, String>();
		TwitchURLs.put(88752545, "http://www.twitch.tv/pixeltailgames");
		TwitchURLs.put(2185137,  "http://www.twitch.tv/macdguy");
		TwitchURLs.put(6854939,  "http://www.twitch.tv/foohy");
		TwitchURLs.put(32039860, "http://www.twitch.tv/gamedev11");
		TwitchURLs.put(22899421, "http://www.twitch.tv/lifeless2011");
		TwitchURLs.put(46510960, "http://www.twitch.tv/zakblystone");
		
		// User Information
		Map <Long, Long> UserCooldown = new HashMap<Long, Long>();
		
		try
		{
			DiscordClient.get().login(Username, Password);
			
			DiscordHelper.write(Credentials.getDebugChannel(), "Samaritan Online");
			
			Thread tTwitchThread = new Thread(new TwitchThread(channelID, TwitchNames, TwitchURLs));
			tTwitchThread.start();
			
			Thread tTrelloThread = new Thread(new TrelloThread(channelID, Trello.getEndTime()));
			tTrelloThread.start();
			
			DiscordClient.get().getDispatcher().registerListener(new IListener<MessageReceivedEvent>() {
				@Override public void receive(MessageReceivedEvent messageReceivedEvent) {
					Message m = messageReceivedEvent.getMessage();
					String messageContent = m.getContent();
					long authorID = Long.parseLong(m.getAuthor().getID());
					
					// Check if user can speak.
					if (DiscordHelper.commandCheck(messageContent)) {
						if (UserCooldown.get(authorID) != null)
						{
							long userTimestamp = UserCooldown.get(authorID);
							if (System.currentTimeMillis() - userTimestamp > 30000) 
							{
								UserCooldown.put(authorID, System.currentTimeMillis());
								System.out.println(authorID + " was allowed to speak.");
							}
							else
							{
								System.out.println(authorID + " can't speak yet.");
								return;
							}
						}
						else
						{
							UserCooldown.put(authorID, System.currentTimeMillis());
							System.out.println(authorID + " has been added to the list.");
						}
					}
					
					// Indiegogo Funding
					if (messageContent.startsWith("!igg"))
					{
						try 
						{
							DiscordHelper.write(channelID, indiegogo);
						} 
						catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
					}
					
					// Samaritan Info Lookup
					if (messageContent.startsWith("!info-samaritan"))
					{
						try 
						{
							DiscordHelper.write(channelID, info);
						} 
						catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
					}
					
					// Samaritan Help
					if (messageContent.startsWith("!help"))
					{
						try 
						{
							DiscordHelper.write(channelID, helpMessage);
						} 
						catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
					}

					// Twitch Lookup
					if (messageContent.startsWith("!twitch"))
					{
						ArrayList<Integer> twitchResult = null;
						try 
						{
							twitchResult = Twitch.check(channelID, TwitchNames, TwitchURLs);
						} 
						catch (ParseException | IOException | InterruptedException e1) { e1.printStackTrace(); }
						
						if (twitchResult.size() == 0) 
						{
							String[] twitchInfo = {"``[Twitch]`` No Pixeltail developers are currently streaming."};
							
							try 
							{
								DiscordHelper.write(channelID, twitchInfo);
							} 
							catch (IOException | InterruptedException e) { e.printStackTrace(); }
						}
					}

					// Dictionary Search
					if (messageContent.startsWith("!define "))
					{
						if (messageContent.substring(8).length() > 0) 
						{
							String[] dictionaryResult = null;
							try 
							{
								dictionaryResult = Dictionary.lookup(messageContent.substring(8));
							} 
							catch (UnsupportedEncodingException e) { e.printStackTrace(); } catch (SAXException e) { e.printStackTrace();
							} 
							catch (IOException e) { e.printStackTrace(); } catch (ParserConfigurationException e) { e.printStackTrace(); }
							
							try 
							{ 
								DiscordHelper.write(channelID, dictionaryResult); 
							} 
							catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
						}
					}
					
					// Google Search
					if (messageContent.startsWith("!g "))
					{
						if (messageContent.substring(3).length() > 0) 
						{
							String[] googleResult = null;
							try 
							{
								googleResult = GoogleSearch.search(messageContent.substring(3));
							} 
							catch (UnsupportedEncodingException e) { e.printStackTrace(); } catch (ParseException e) { e.printStackTrace(); }
							
							try 
							{ 
								DiscordHelper.write(channelID, googleResult); 
							} 
							catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) {	e.printStackTrace(); }
						}
					}
					
					// Google Image Search
					if (messageContent.startsWith("!gi "))
					{
						if (messageContent.substring(4).length() > 0) 
						{
							String[] googleImageResult = null;
							try 
							{
								googleImageResult = GoogleImageSearch.search(messageContent.substring(4));
							} 
							catch (UnsupportedEncodingException e) { e.printStackTrace(); } catch (ParseException e) { e.printStackTrace(); }
							
							try 
							{ 
								DiscordHelper.write(channelID, googleImageResult); 
							} 
							catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
						}
					}
					
					// YouTube Search
					if (messageContent.startsWith("!yt "))
					{
						if (messageContent.substring(4).length() > 0) 
						{
							String[] youtubeResult = null;
							try 
							{
								youtubeResult = YouTubeSearch.search(messageContent.substring(4));
							} 
							catch (UnsupportedEncodingException e) { e.printStackTrace(); } catch (ParseException e) { e.printStackTrace(); }
							
							try 
							{ 
								DiscordHelper.write(channelID, youtubeResult); 
							} 
							catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
						}
					}
					
					// Weather
					if (messageContent.startsWith("!w ")) 
					{
						if (messageContent.substring(3).length() > 0) 
						{
							String[] weatherResult = null;
							try 
							{
								weatherResult = Weather.get(messageContent.substring(3));
							} 
							catch (UnsupportedEncodingException e) { e.printStackTrace(); } catch (ParseException e) { e.printStackTrace(); }
							
							try 
							{ 
								DiscordHelper.write(channelID, weatherResult); 
							} 
							catch (IOException e) { e.printStackTrace(); } catch (InterruptedException e) { e.printStackTrace(); }
						}
					}
					
					// Suspend
					if (m.getContent().startsWith("!suspend") && m.getAuthor().getID().equals("93949605548392448")) 
					{
						try 
						{ 
							DiscordHelper.write(Credentials.getDebugChannel(), "Samaritan Offline"); 
						} 
						catch (IOException e) { e.printStackTrace(); }
						
						try 
						{ 
							// Record Time to File for Trello
							Trello.recordEndTime(); 
						} 
						catch (IOException e) { e.printStackTrace(); }
						
						// End Program
						System.exit(0);
					}
					
					// Get Discord User ID
					if (m.getContent().startsWith(".test")) {
						try {
							DiscordHelper.write(channelID, m.getAuthor().getID());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			});
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}